/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pyramidapp;

/**
 *
 * @author ADMIN
 */
public class Product {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        int productID = 19845; //khai báo biến nguyên : int productID = 19845;
        /*Biến 'productID' được khai báo và khởi tạo để lưu trữ 
        một giá trị nguyên đại diện cho mã nhận dạng sản phẩm*/
        String productName = "MensTrouser";// khai báo1 biến kiểu String
        /*Biến 'productName' đại diện cho tên của sản phẩm và được khởi tạo bằng 
          giá trị chuỗi*/
        /*String là một class trong Java nhưng cũng có thể sử dụng như một kiểu
        dữ liệu để khai báo biến*/
        char productType = 'A'; //khai báo biến kiểu ký tự
        //*Biến 'productType' đại diện cho danh mục của sản phẩm
        short quantity = 148; //Khai báo kiểu số nguyên ngắn
        //'quantity' : thể hiện số lượng có sẵn trong kho và được khởi tạo với giá trị ngắn
        double productPrice = 15_25_4.65;//khai báo biến kiểu dấu phẩy động
        /*'prooductPrice' đại diện cho giá của sản phẩm. Nó đã được khởi tạo 
        một giá trị kép*/
        
        int barcodeNumber = 0b1101_0101_0001_1010;//khai báo một kiểu số nguyên để lưu giá trị nhị phân

        /*barcodeNumber: có thể thay đổi số đại diện cho mã vạch của sản phẩm
        nó đã được khởi tạo bởi một giá trị nhị phân được nhóm theo đơn vị bốn chữ số*/

        boolean availableStatus = true;//khai báo biến kiểu boolean

        /*biến 'availableStatus' được khởi tạo bởi giá trị logic true cho biết
        rằng sản phẩm có sẵn trong kho*/
        
        System.out.println("\tProduct Details");//46,47 hiển thị
        System.out.println("------------------");
        System.out.println("Product Identification Code: " + productID);//48-52 hien thi cac bien khai bao o tren
        System.out.println("Product Name: " + productName);
        System.out.println("Product Category (A: Apparels G: Gadgets): " + 
productType);
        System.out.println("Product Quantity: " + quantity);
        System.out.println("Product Price: $" + productPrice);
        System.out.println("Product BarCode Number: " + barcodeNumber);
        System.out.println("Product Avaulability: " + availableStatus);
    }
//biên dịch lớp và sản phẩm ẤN : SHIFT+F11 SAU ĐÓ SHIFT+F6 ĐỂ CHẠY
}
