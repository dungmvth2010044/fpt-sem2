/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package pyramidapp;

/**
 *
 * @author ADMIN
 */
import java.util.Scanner;//thêm câu lệnh nhập cho Scanner class
/*
Câu lệnh được tạo và thêm vào mã. Nó thông báo cho trình biên dịch về các lớp
được sử dụng trong trương trình */

public class Customer {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        //21-24 :khai báo các biến để lưu trữ thông tin chi tiết về khách hàng
        long customerCode;
        String customerName;
        int customerAge;
        double depositAmount;
        System.out.println("Enter the details for the new customer.");
        Scanner input = new Scanner(System.in);//tạo 1 đối tượng thuộc lớp Scanner
        /*lớp Scanner lấy System.in làm tham số. System.in đại diện cho một luồng đầu
vào cho phép bạn đọc các giá trị từ bàn phím*///->chú ý: check dòng 2 rồi quay lại
        System.out.println("Enter the Four-digit Customer Code: ");
        customerCode = input.nextLong();//để chấp nhận giá trị cho mã nhận khách hàng
        /*đối tượng đầu vào của Scanner class đọc đầu vào và trả về dưới dạng giá
trị dài*/
        System.out.println("Enter the Customer First Name: ");
        customerName = input.next();//chấp nhận giá trị cho tên khách hàng
/*đối tượng đầu vào của Scanner class đọc đầu vào và trả về dưới dạng giá
trị chuỗi*/
        System.out.println("Enter the Customer Age: ");
        customerAge = input.nextInt();//để chấp nhận giá trị cho độ tuổi của khác hàng
/*đối tượng đầu vào của Scanner class đọc đầu vào và trả về dưới dạng giá
trị số nguyên*/

        System.out.println("Enter the Amount Deposited:");
        depositAmount = input.nextDouble();//chấp nhận giá trị của số tiền gửi
        /*đối tượng đầu vào của Scanner class đọc đầu vào và trả về dưới dạng giá
trị kép*/
        //Hiển thị giá trị của các biến
        
        System.out.println("\nCustomerCode \t\t CustomerName \t\t CustomerAge\t\t DepositAmount");
        System.out.println("-----------------------------------------------------");
//53-54 hiển thị các tiêu dề dưới dạng thông báo trên bảng điều khiển

        System.out.format("%08d \t\t\t", customerCode);//cung cấp một phương thức định dạng các giá trị trong đầu ra
        /*'format() định dạng các đối số dựa trên một chuỗi định dạng'*/
    // chuỗi định dạng 08d chỉ định rằng customerCode sẽ được hiển thị trong 8 cột được đệm bằng các số 0 ở đầu
    // \t in các tab ngang trên bảng điều khiển, theo sau là customerCode có giá trị được định dạng
        System.out.format("%s \t\t\t", customerName);
        System.out.format("%d \t\t\t", customerAge);
        System.out.format("%.2f\n", depositAmount);
    }

}
