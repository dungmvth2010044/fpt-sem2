import java.util.List;
import java.util.Arrays;

public class Driver {

    public Driver(double width, double height, String color, boolean filled,double radius){
        super(width,height,color,filled,radius);
    }
    

    public static void main(String[] args) {
        
        GeometricObject geometry = new GeometricObject();
        geometry.getDateCreated();

        Circle circle = new Circle();
        circle.setColor("red");
        circle.setFilled(true);
        circle.setRadius(12);//bien tam thoi

        Rectangle rectangle = new Rectangle(2, 3, "red", true);
        rectangle.setHeight(2);
        rectangle.setWidth(4);

        System.out.println("Circle : { Color = " + circle.getColor() + "  |  Radius = " + circle.getRadius() + " | Diameter = " + circle.getDiameter() + "  |  Perimeter = " + circle.getPerimeter() + "  |  Area = " + circle.getArea() + "  }");
        System.out.println("Rectangle : { Color = " + circle.getColor() + "  |  Height = " + rectangle.getHeight() + " | Width = " + rectangle.getWidth() + "  |  Perimeter = " + rectangle.getPerimeter() + "  |  Area = " + rectangle.getArea() + "  }");


    }
    
}
