public class Account {

    //Declare a variable to store account number
    String accountNumber;
    //Declare a variable to store type of account balance
    String accountHolderName;
    String accountType;
    double balance;

    pubic Account() {
        System.out.println("Default Constructor Invoked..");
        accountNumber = "Not Specified";
        accountHoldderName = "Not Specified";
        accountType = "Not Specified";
        balance = 0.0;
    }
    pubic Account(String accountNumber, String accountHolderName, 
    String accountType, double balance){
        System.out.println("Parameterized Constructor Invoked");
        this.accountNumber = accountNumber;
        this.accountHolderName = accountHolderName;
        this.accountType = accountType;
        this.balance = balance;

    }
    public static void main(String[] args) {
        Account objAccount1;
        objAccount1 = new Account();

        System.out.println("Account Number: " + objAccount1.accountNumber);
        System.out.println("Account Holder Name: " + objAccount1.accountHolderName);
        System.out.println("Account Type: " +objAccount1.accountType);
        System.out.println("Balance: " + objAccount1.balance);
        System.out.println("==================================================");

        Account objAccount2 = new Account("Acc001", "John Smith", "Savings",6500.789);

        System.out.println("Account Number: " + objAccount2.accountNumber);
        System.out.println("Account Holder Name: " + objAccount2.accountHolderName);
        System.out.println("Account Type: " +objAccount2.accountType);
        System.out.println("Balance: " + objAccount2.balance);
    }
    
}
