public class Account_2 {
    //Declare a variable to store account number
    private String accountNumber;
    //Declare a variable to store type of account balance
    private String accountHolderName;
    private String accountType;
    private double balance;

    pubic Account() {
        System.out.println("Default Constructor Invoked..");
        accountNumber = "Not Specified";
        accountHoldderName = "Not Specified";
        accountType = "Not Specified";
        balance = 0.0;
    }
    pubic Account(String accountNumber, String accountHolderName, 
    String accountType, double balance){
        System.out.println("Parameterized Constructor Invoked");
        this.accountNumber = accountNumber;
        this.accountHolderName = accountHolderName;
        this.accountType = accountType;
        this.balance = balance;

    }

    public String getAccountNumber(){
        return accountNumber;
    }

    public void setAccountNumber (String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountHolderName() {
        return accountHolderName;
    }
    
}