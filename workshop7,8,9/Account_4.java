public class Account_4 {
    public class Account_2 {
        //Declare a variable to store account number
        private String accountNumber;
        //Declare a variable to store type of account balance
        private String accountHolderName;
        private String accountType;
        private double balance;
    
        pubic Account() {
            System.out.println("Default Constructor Invoked..");
            accountNumber = "Not Specified";
            accountHoldderName = "Not Specified";
            accountType = "Not Specified";
            balance = 0.0;
        }
        pubic Account(String accountNumber, String accountHolderName, 
        String accountType, double balance){
            System.out.println("Parameterized Constructor Invoked");
            this.accountNumber = accountNumber;
            this.accountHolderName = accountHolderName;
            this.accountType = accountType;
            this.balance = balance;
    
        }
    
        public String getAccountNumber(){
            return accountNumber;
        }
    
        public void setAccountNumber (String accountNumber) {
            this.accountNumber = accountNumber;
        }
    
        public String getAccountHolderName() {
            return accountHolderName;
        }
        public double getBalance() {
            return balance;
        }
        public void setBalance(double balance) {
            this.balance = balance;
        }
        public void displayDetails(){
            System.out.println("Account Detail of Customer");
            System.out.println("Account Number: " + accountNumber );
            System.out.println("Accout Holder Name: " + accountHolderName);
            System.out.println("Account Type: " + accountType);
            System.out.println("Balance: " + balance);
        }
    }
}

