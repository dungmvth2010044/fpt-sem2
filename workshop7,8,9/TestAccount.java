public class TestAccount {
    public static void main(String[] args) {
        Account objAccount1 = new Account();
        objAccount1.setAccountNumber("Acc001");
        objAccount1.setAccountType("Savings");
        objAccount1.setBalance("6500.789");
        System.out.println("Account Details");
        System.out.println("Account Number: " + objAccount1.getAccountNumber());
        System.out.println("Account Holder Name: " + objAccount1.getAccountHolderName());
        System.out.println("Account Type: " + objAccount1.getAccountType());
        System.out.println("Balance: " + objAccount1.getBalance());
        
    }
}
