/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package java_trenlop;

/**
 *
 * @author ADMIN
 */
public class FlashLamp {

    /**
     * @param args the command line arguments
     */
    /*
    Fields'lĩnh vực'
    */
    private boolean status;
    private Battery battery;
// Đặt đối tượng Pin vào FlashLamp vẫn chạy tốt -> LTHDDT
         // Hàm tạo cho đối tượng của lớp Flashlamp
    public FlashLamp()
{
        //to do:
        status=false;
}

/*
    Methods'phuong phap'
*/
    public void setBattery(Battery battery) {
        this.battery=battery; 
}
    public int getBatteryInfo() {
        return battery.getEnergy();
}
    public void light() {
        if(status==true&&battery!=null&&battery.getEnergy()>0) {
            battery.decreaseEnergy();
        }
    }
    public void turnOn() {
        if(battery!=null&&battery.getEnergy()>0){
            status=true;
        }
        light();
    }
    public void turnOff() {
        status=false;
    }
}
