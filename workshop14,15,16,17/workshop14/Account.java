public class Account {
    // Create private fiels
    private String userId;
    private String password;

    // Create getter and setter methods

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Account() {} 
    @Override
    public String toString() { // override the inherited toString() method to dis play user id and password
        return "Account{ " + "userId=" + userId + ", + password=" + password + '}'; 

    }
}
